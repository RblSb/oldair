package game;

import kha.graphics2.Graphics;
import kha.input.KeyCode;
import kha.Image;
import kha.math.FastMatrix3;
import kha.FastFloat;
import game.Collision.Collision;
import game.Collision.UpdateTileCollision;
import game.Collision.UpdatePosition;
import khm.Types.Point;
import khm.Types.Size;
import khm.utils.Utils;
using khm.utils.MathExtension;

enum PlaneState {
	Standing;
	Langing;
	Lifting;
	Flight;
}

class Plane {

	var game:Game;
	public var pos:Point;
	public var size:Size;
	var wantedForce:Float;
	var force:Float;
	var angle:Float;
	var speed:Point;
	var lastSpeed:Point;
	var coll:Collision;
	var gravity:Float;
	var img:Image;
	var state:PlaneState;
	var updateTileCollision = new UpdateTileCollision();
	var updatePosition = new UpdatePosition();
	var maxForce = 3.0;
	var langingY = 0.0;
	var maxLangingY = 2.0;
	var langingAngle = -15.0;

	public function new(game:Game, img:Image, x:Float, y:Float) {
		this.game = game;
		this.img = img;
		pos = {
			x: x,
			y: y
		};
		size = {
			w: img.width,
			h: frameH
		};
		coll = {
			state: false,
			left: false,
			right: false,
			up: false,
			down: false
		};
		state = Standing;
		force = 0;
		angle = 0;
		speed = {
			x: 0,
			y: 0
		};
		lastSpeed = {
			x: 0,
			y: 0
		};
		gravity = 0;
	}

	function control():Void {}

	public function update():Void {
		control();
		var forceRatio = force / maxForce;
		if (wantedForce > 0) {
			if (frame < frames.length - 1) frame++;
			else frame = 0;
		}

		speed.x = force * Math.cos(angle.toRad());
		speed.y = force * Math.sin(angle.toRad());
		var liftForce = 0.15;
		speed.y += liftForce - forceRatio * liftForce * 2;

		if (!coll.down) {
			if (angle > 180 && angle < 360) {
				var diff = Math.abs(270 - angle);
				var ratio = (90 - diff) / 90;
				force -= ratio / 13;
				if (force < 0) force = 0;
			}
			if (angle > 0 && angle < 180) {
				var diff = Math.abs(90 - angle);
				var ratio = (90 - diff) / 90;
				force += ratio / 10;
			}
		}

		gravity += (1 - forceRatio) / 10 - forceRatio / 15;
		if (gravity < 0) gravity = 0;
		if (gravity > 4) gravity = 4;
		speed.y += gravity;

		coll = updateTileCollision.update(pos, size, speed);
		if (coll.down) {
			gravity = 0;
			if (speed.x <= lastSpeed.x && speed.x < 2) state = Langing;
			else if (speed.x > 0.3) state = Lifting;

			if (state == Langing) {
				var dist = Utils.distAng(angle, langingAngle);
				angle += sign(Std.int(dist));
				if (langingY < maxLangingY) langingY += 0.2;
				if (langingY >= maxLangingY) {
					langingY = maxLangingY;
					state = Standing;
				}
			}
			if (state == Lifting) {
				var dist = Utils.distAng(angle, 0);
				angle += dist > 0 ? 1 : -1;
				if (langingY > 0) langingY -= 0.2;
				if (langingY <= 0) {
					langingY = 0;
					state = Flight;
				}
			}
		}
		lastSpeed.x = speed.x;
		lastSpeed.y = speed.y;
		updatePosition.update(pos, speed);
	}

	inline function sign(n:Float):Float {
		if (n == 0) return 0;
		return n > 0 ? 1 : -1;
	}

	var tempMatrix = FastMatrix3.identity();
	var frames = [0, 1, 2, 1];
	var frame = 0;
	var frameH = 14;

	function drawHP(g:Graphics):Void {}

	public function render(g:Graphics):Void {
		var camera = game.tilemap.camera;
		tempMatrix.setFrom(g.transformation);
		g.transformation = g.transformation.multmat(
			FastMatrix3.translation(camera.x, camera.y)
		).multmat(
			rotation(angle.toRad(), pos.x + size.w / 2, pos.y + size.h / 2)
		);
		//g.color = 0xFF00FFFF;
		//g.drawRect(pos.x, pos.y, size.w, size.h);
		g.color = 0xFFFFFFFF;
		//g.drawImage(img, pos.x, pos.y + langingY);
		var fy = frames[frame] * frameH;
		g.drawSubImage(
			img, pos.x, pos.y + langingY,
			0, fy, img.width, frameH
		);
		g.transformation = tempMatrix;
		drawHP(g);
		// var ang = Math.atan2(speed.y, speed.x);
		// if (ang < 0) ang += 2 * Math.PI;
		// var x = pos.x + size.w / 2 + camera.x;
		// var y = pos.y + size.h / 2 + camera.y;
		// g.drawLine(
		// 	x, y,
		// 	x + Math.cos(ang) * size.w,
		// 	y + Math.sin(ang) * size.w
		// );
	}

	inline function rotation(angle: FastFloat, centerx: FastFloat, centery: FastFloat): FastMatrix3 {
		return FastMatrix3.translation(centerx, centery)
			.multmat(FastMatrix3.rotation(angle))
			.multmat(FastMatrix3.translation(-centerx, -centery));
	}

}
