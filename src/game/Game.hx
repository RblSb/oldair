package game;

import kha.graphics2.Graphics;
import kha.input.KeyCode;
import kha.Canvas;
import kha.Assets;
import khm.editor.Editor;
import khm.Screen;
import khm.Screen.Pointer;
import khm.tilemap.Tilemap;
import khm.tilemap.Tilemap.GameMapJSON;
import khm.tilemap.Tileset;

class Game extends Screen {

	public static var lvl:Tilemap;
	public var tilemap:Tilemap;
	var planes:Array<Plane>;
	var player:Plane;

	public function init():Void {
		CustomData.init();
		lvl = tilemap = new Tilemap();
		var tileset = new Tileset(Assets.blobs.tiles_json);
		tilemap.init(tileset);
		loadMap(0);
		planes = [];
		planes.push(new Player(this, Assets.images.planes_0, 0, 0));
		planes.push(new Bot(this, Assets.images.planes_0, 0, tilemap.tileSize * 5));
		player = planes[0];
		// var editor = new Editor();
		// editor.init();
		// editor.show();
		//setScale(2);
	}

	public static inline function isMapExists(id:Int):Bool {
		return Assets.blobs.get("maps_" + id + "_json") != null;
	}

	public function loadMap(id:Int):Void {
		var data = Assets.blobs.get("maps_" + id + "_json");
		tilemap.loadJSON(haxe.Json.parse(data.toString()));
	}

	override function onKeyDown(key:KeyCode):Void {
		if (key == 189 || key == KeyCode.HyphenMinus) {
			if (scale > 1) setScale(scale - 0.5);

		} else if (key == 187 || key == KeyCode.Equals) {
			if (scale < 9) setScale(scale + 0.5);
		}
	}

	override function onResize():Void {
		tilemap.camera.w = Screen.w;
		tilemap.camera.h = Screen.h;
	}

	override function onRescale(scale:Float):Void {
		tilemap.scale = scale;
	}

	override function onUpdate():Void {
		for (plane in planes) plane.update();
		tilemap.camera.strictCenter({
			x: player.pos.x,
			y: player.pos.y,
			w: player.size.w,
			h: player.size.h
		});
	}

	override function onRender(canvas:Canvas):Void {
		var g = canvas.g2;
		g.begin();
		g.font = Assets.fonts.OpenSans_Regular;
		g.color = 0xFFBEE6FF;
		g.fillRect(0, 0, Screen.w, Screen.h);
		g.color = 0xFFFFFFFF;
		tilemap.drawLayers(g);
		drawTileset(g);
		for (plane in planes) plane.render(g);
		g.end();
	}

	function drawTileset(g:Graphics):Void {
		#if debug
		var tileset = @:privateAccess tilemap.tileset;
		var scale = 1;
		var x = tilemap.camera.w - tileset.img.width * scale;
		g.drawScaledImage(
			tileset.img, x, 0,
			tileset.img.width * scale,
			tileset.img.height * scale
		);
		#end
	}

}
