package game;

import kha.graphics2.Graphics;
import kha.input.KeyCode;
import kha.Image;
import kha.math.FastMatrix3;
import kha.FastFloat;
import game.Collision.Collision;
import game.Collision.UpdateTileCollision;
import game.Collision.UpdatePosition;
import khm.Types.Point;
import khm.Types.Size;
import khm.utils.Utils;
using khm.utils.MathExtension;

class Player extends Plane {

	public function new(game:Game, img:Image, x:Float, y:Float) {
		super(game, img, x, y);
	}

	override function control():Void {
		var keys = game.keys;
		if (keys[Up]) {
			wantedForce += 0.1;
			if (wantedForce > maxForce) wantedForce = maxForce;
		}
		if (keys[Down]) {
			wantedForce -= 0.1;
			if (wantedForce < 0) wantedForce = 0;
		}
		if (force < wantedForce) {
			force += 0.05;
			if (force > wantedForce) force = wantedForce;
		}
		if (force > wantedForce) {
			force -= 0.05;
			if (force < wantedForce) force = wantedForce;
		}

		if (!coll.down || force > maxForce / 2) {
			if (keys[Left]) angle -= 3;
			if (keys[Right]) angle += 3;
		}
		if (angle < 0) angle += 360;
		if (angle > 359) angle -= 360;
	}

	override function drawHP(g:Graphics):Void {
		g.color = 0xFF000000;
		var x = 10;
		var y = 10;
		var w = 70;
		var h = 10;
		g.drawRect(x, y, w, h);
		var color:kha.Color = 0xFF00FF00;
		for (i in 0...7) {
			g.color = color;
			g.fillRect(x + i * w / 7, y, w / 7, h);
			color.G -= 1 / 7;
			color.R += 1 / 7;
		}
		g.color = 0xFF000000;
		var forceRatio = wantedForce / maxForce;
		g.fillRect(x + w * forceRatio, y - 2, 2, h + 2 * 2);
		g.color = 0xFF0000FF;
		var forceRatio = force / maxForce;
		g.fillRect(x + w * forceRatio, y - 2, 2, h + 2 * 2);
	}

}
