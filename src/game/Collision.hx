package game;

import kha.graphics2.Graphics;
import kha.Assets;
import kha.Image;
import khm.tilemap.Tilemap;
import game.CustomData.Slope;
import khm.Types.Point;
import khm.Types.Size;
import khm.Types.Rect;

typedef Collision = {
	state:Bool,
	left:Bool,
	right:Bool,
	up:Bool,
	down:Bool
}

class UpdateTileCollision {

	var tsize(get, never):Int;
	function get_tsize() return lvl.tileSize;
	var lvl(get, never):Tilemap;
	function get_lvl() return Game.lvl;

	public function new() {}

	public function update(pos:Point, size:Size, speed:Point):Collision {
		//if (pos.fixed) return;
		var coll:Collision = {
			state: false,
			left: false,
			right: false,
			up: false,
			down: false
		}

		var sx = Math.abs(speed.x);
		var sy = Math.abs(speed.y);
		var vx = sx / speed.x;
		var vy = sy / speed.y;
		var min = tsize / 4;

		while(sx > min || sy > min) {
			if (sx > min) {
				pos.x += min * vx;
				sx -= min;
				collision(coll, pos, size, speed, 0);
				if (coll.state) sx = 0;
			}

			if (sy > min) {
				pos.y += min * vy;
				sy -= min;
				collision(coll, pos, size, speed, 1);
				if (coll.state) sy = 0;
			}
		}

		if (sx > 0) {
			pos.x += sx * vx;
			collision(coll, pos, size, speed, 0);
		}

		if (sy > 0) {
			pos.y += sy * vy;
			collision(coll, pos, size, speed, 1);
		}

		pos.x -= speed.x;
		pos.y -= speed.y;
		return coll;
	}

	function collision(coll:Collision, pos:Point, size:Size, speed:Point, dir:Int):Void {
		var rect:Rect = {
			x: pos.x,
			y: pos.y,
			w: size.w,
			h: size.h
		};
		var x = Std.int(rect.x / tsize);
		var y = Std.int(rect.y / tsize);
		var maxX = Math.ceil((rect.x + rect.w) / tsize);
		var maxY = Math.ceil((rect.y + rect.h) / tsize);

		for (iy in y...maxY) {
			for (ix in x...maxX) {
				//var tile = lvl.getProps(1, ix, iy);
				var tile = lvl.getTile(1, ix, iy).props;
				if (tile.collide) block(ix, iy, coll, pos, size, speed, dir, tile.type);
			}
		}
	}

	inline function block(ix:Int, iy:Int, coll:Collision, pos:Point, size:Size, speed:Point, dir:Int, slope:Slope):Void {
		switch (slope) {
			case FULL: FULL(ix, iy, coll, pos, size, speed, dir);
			case HALF_B: HALF_B(ix, iy, coll, pos, size, speed, dir);
			case HALF_T: HALF_T(ix, iy, coll, pos, size, speed, dir);
			case HALF_L: HALF_L(ix, iy, coll, pos, size, speed, dir);
			case HALF_R: HALF_R(ix, iy, coll, pos, size, speed, dir);
			case HALF_BL: HALF_BL(ix, iy, coll, pos, size, speed, dir);
			case HALF_BR: HALF_BR(ix, iy, coll, pos, size, speed, dir);
			case HALF_TL: HALF_TL(ix, iy, coll, pos, size, speed, dir);
			case HALF_TR: HALF_TR(ix, iy, coll, pos, size, speed, dir);
			case QUARTER_BL: QUARTER_BL(ix, iy, coll, pos, size, speed, dir);
			case QUARTER_BR: QUARTER_BR(ix, iy, coll, pos, size, speed, dir);
			case QUARTER_TL: QUARTER_TL(ix, iy, coll, pos, size, speed, dir);
			case QUARTER_TR: QUARTER_TR(ix, iy, coll, pos, size, speed, dir);
			default: trace(slope);
		}
	}

	inline function tileLeft(ix:Int, iy:Int, coll:Collision, pos:Point, size:Size, speed:Point) {
		coll.state = true;
		coll.left = true;
		pos.x = ix * tsize + tsize;
		speed.x = 0;
	}

	inline function tileRight(ix:Int, iy:Int, coll:Collision, pos:Point, size:Size, speed:Point) {
		coll.state = true;
		coll.right = true;
		pos.x = ix * tsize - size.w;
		speed.x = 0;
	}

	inline function tileBottom(ix:Int, iy:Int, coll:Collision, pos:Point, size:Size, speed:Point) {
		coll.state = true;
		coll.down = true;
		pos.y = iy * tsize - size.h;
		speed.y = 0;
	}

	inline function tileTop(ix:Int, iy:Int, coll:Collision, pos:Point, size:Size, speed:Point) {
		coll.state = true;
		coll.up = true;
		pos.y = iy * tsize + tsize;
		speed.y = 0;
	}

	inline function FULL(ix:Int, iy:Int, coll:Collision, pos:Point, size:Size, speed:Point, dir:Int) {
		if (dir == 0) {
			if (speed.x < 0) tileLeft(ix, iy, coll, pos, size, speed);
			else if (speed.x > 0) tileRight(ix, iy, coll, pos, size, speed);
		} else if (dir == 1) {
			if (speed.y > 0) tileBottom(ix, iy, coll, pos, size, speed);
			else if (speed.y < 0) tileTop(ix, iy, coll, pos, size, speed);
		}
	}

	inline function HALF_B(ix:Int, iy:Int, coll:Collision, pos:Point, size:Size, speed:Point, dir:Int) {
		if (pos.y + size.h > iy * tsize + tsize / 2) {
			if (dir == 0) {
				if (speed.x < 0) tileLeft(ix, iy, coll, pos, size, speed);
				else if (speed.x > 0) tileRight(ix, iy, coll, pos, size, speed);
			} else if (dir == 1) {
				if (speed.y > 0) {
					coll.state = true;
					coll.down = true;
					pos.y = iy * tsize + tsize / 2 - size.h;
					speed.y = 0;
				} else if (speed.y < 0) tileTop(ix, iy, coll, pos, size, speed);
			}
		}
	}

	inline function HALF_T(ix:Int, iy:Int, coll:Collision, pos:Point, size:Size, speed:Point, dir:Int) {
		if (pos.y < iy * tsize + tsize / 2) {
			if (dir == 0) {
				if (speed.x < 0) tileLeft(ix, iy, coll, pos, size, speed);
				else if (speed.x > 0) tileRight(ix, iy, coll, pos, size, speed);
			} else if (dir == 1) {
				if (speed.y > 0) tileBottom(ix, iy, coll, pos, size, speed);
				else if (speed.y < 0) {
					coll.state = true;
					coll.up = true;
					pos.y = iy * tsize + tsize / 2;
					speed.y = 0;
				}
			}
		}
	}

	inline function HALF_L(ix:Int, iy:Int, coll:Collision, pos:Point, size:Size, speed:Point, dir:Int) {
		if (pos.x < ix * tsize + tsize / 2) {
			if (dir == 0) {
				if (speed.x < 0) {
					coll.state = true;
					coll.left = true;
					pos.x = ix * tsize + tsize / 2;
					speed.x = 0;
				} else if (speed.x > 0) tileRight(ix, iy, coll, pos, size, speed);
			} else if (dir == 1) {
				if (speed.y > 0) tileBottom(ix, iy, coll, pos, size, speed);
				else if (speed.y < 0) tileTop(ix, iy, coll, pos, size, speed);
			}
		}
	}

	inline function HALF_R(ix:Int, iy:Int, coll:Collision, pos:Point, size:Size, speed:Point, dir:Int) {
		if (pos.x + size.w > ix * tsize + tsize / 2) {
			if (dir == 0) {
				if (speed.x < 0) tileLeft(ix, iy, coll, pos, size, speed);
				else if (speed.x > 0) {
					coll.state = true;
					coll.right = true;
					pos.x = ix * tsize + tsize / 2 - size.w;
					speed.x = 0;
				}
			} else if (dir == 1) {
				if (speed.y > 0) tileBottom(ix, iy, coll, pos, size, speed);
				else if (speed.y < 0) tileTop(ix, iy, coll, pos, size, speed);
			}
		}
	}
	//TODO implement triangle collision
	inline function HALF_BL(ix:Int, iy:Int, coll:Collision, pos:Point, size:Size, speed:Point, dir:Int) {}

	inline function HALF_BR(ix:Int, iy:Int, coll:Collision, pos:Point, size:Size, speed:Point, dir:Int) {}

	inline function HALF_TL(ix:Int, iy:Int, coll:Collision, pos:Point, size:Size, speed:Point, dir:Int) {}

	inline function HALF_TR(ix:Int, iy:Int, coll:Collision, pos:Point, size:Size, speed:Point, dir:Int) {}

	inline function QUARTER_BL(ix:Int, iy:Int, coll:Collision, pos:Point, size:Size, speed:Point, dir:Int) {
		if (pos.x < ix * tsize + tsize / 2 &&
			pos.y + size.h > iy * tsize + tsize / 2) {
			if (dir == 0) {
				if (speed.x < 0) {
					coll.state = true;
					coll.left = true;
					pos.x = ix * tsize + tsize / 2;
					speed.x = 0;
				} else if (speed.x > 0) tileRight(ix, iy, coll, pos, size, speed);
			} else if (dir == 1) {
				if (speed.y > 0) {
					coll.state = true;
					coll.down = true;
					pos.y = iy * tsize + tsize / 2 - size.h;
					speed.y = 0;
				} else if (speed.y < 0) tileTop(ix, iy, coll, pos, size, speed);
			}
		}
	}

	inline function QUARTER_BR(ix:Int, iy:Int, coll:Collision, pos:Point, size:Size, speed:Point, dir:Int) {
		if (pos.x + size.w > ix * tsize + tsize / 2 &&
			pos.y + size.h > iy * tsize + tsize / 2) {
			if (dir == 0) {
				if (speed.x < 0) tileLeft(ix, iy, coll, pos, size, speed);
				else if (speed.x > 0) {
					coll.state = true;
					coll.right = true;
					pos.x = ix * tsize + tsize / 2 - size.w;
					speed.x = 0;
				}
			} else if (dir == 1) {
				if (speed.y > 0) {
					coll.state = true;
					coll.down = true;
					pos.y = iy * tsize + tsize / 2 - size.h;
					speed.y = 0;
				} else if (speed.y < 0) tileTop(ix, iy, coll, pos, size, speed);
			}
		}
	}

	inline function QUARTER_TL(ix:Int, iy:Int, coll:Collision, pos:Point, size:Size, speed:Point, dir:Int) {
		if (pos.x < ix * tsize + tsize / 2 &&
			pos.y < iy * tsize + tsize / 2) {
			if (dir == 0) {
				if (speed.x < 0) {
					coll.state = true;
					coll.left = true;
					pos.x = ix * tsize + tsize / 2;
					speed.x = 0;
				} else if (speed.x > 0) tileRight(ix, iy, coll, pos, size, speed);
			} else if (dir == 1) {
				if (speed.y > 0) tileBottom(ix, iy, coll, pos, size, speed);
				else if (speed.y < 0) {
					coll.state = true;
					coll.up = true;
					pos.y = iy * tsize + tsize / 2;
					speed.y = 0;
				}
			}
		}
	}

	inline function QUARTER_TR(ix:Int, iy:Int, coll:Collision, pos:Point, size:Size, speed:Point, dir:Int) {
		if (pos.x + size.w > ix * tsize + tsize / 2 &&
			pos.y < iy * tsize + tsize / 2) {
			if (dir == 0) {
				if (speed.x < 0) tileLeft(ix, iy, coll, pos, size, speed);
				else if (speed.x > 0) {
					coll.state = true;
					coll.right = true;
					pos.x = ix * tsize + tsize / 2 - size.w;
					speed.x = 0;
				}
			} else if (dir == 1) {
				if (speed.y > 0) tileBottom(ix, iy, coll, pos, size, speed);
				else if (speed.y < 0) {
					coll.state = true;
					coll.up = true;
					pos.y = iy * tsize + tsize / 2;
					speed.y = 0;
				}
			}
		}
	}

}

class UpdatePosition {

	var maxSpeed = Game.lvl.tileSize;

	public function new() {}

	public function update(pos:Point, speed:Point):Void {
		//if (pos.fixed) return;
		if (speed.x > maxSpeed) speed.x = maxSpeed;
		if (speed.x < -maxSpeed) speed.x = -maxSpeed;
		if (speed.y > maxSpeed) speed.y = maxSpeed;
		if (speed.y < -maxSpeed) speed.y = -maxSpeed;
		pos.x += speed.x;
		pos.y += speed.y;
	}

}
