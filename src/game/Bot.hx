package game;

import kha.graphics2.Graphics;
import kha.input.KeyCode;
import kha.Image;
import kha.math.FastMatrix3;
import kha.FastFloat;
import game.Collision.Collision;
import game.Collision.UpdateTileCollision;
import game.Collision.UpdatePosition;
import khm.Types.Point;
import khm.Types.Size;
import khm.utils.Utils;
using khm.utils.MathExtension;

private enum AiState {
	Lifting;
	SearchEnemy;
}

class Bot extends Plane {

	var aiState:AiState;

	public function new(game:Game, img:Image, x:Float, y:Float) {
		aiState = Lifting;
		super(game, img, x, y);
	}

	override function control():Void {
		var keys = new Map<KeyCode, Bool>();
		switch (aiState) {
			case Lifting:
				keys[Up] = true;
				if (!coll.down) aiState = SearchEnemy;
			case SearchEnemy:
				keys[Up] = wantedForce < maxForce;
				keys[Left] = coll.state;
		}
		if (keys[Up]) {
			wantedForce += 0.1;
			if (wantedForce > maxForce) wantedForce = maxForce;
		}
		if (keys[Down]) {
			wantedForce -= 0.1;
			if (wantedForce < 0) wantedForce = 0;
		}
		if (force < wantedForce) {
			force += 0.05;
			if (force > wantedForce) force = wantedForce;
		}
		if (force > wantedForce) {
			force -= 0.05;
			if (force < wantedForce) force = wantedForce;
		}

		if (!coll.down || force > maxForce / 2) {
			if (keys[Left]) angle -= 3;
			if (keys[Right]) angle += 3;
		}
		if (angle < 0) angle += 360;
		if (angle > 359) angle -= 360;
	}

}
